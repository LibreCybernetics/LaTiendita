{-# LANGUAGE TemplateHaskell #-}
module LaTiendita.Common.Route where

import Data.Text (Text)
import Data.Functor.Identity

import Obelisk.Route
import Obelisk.Route.TH

-- Backend

data ApiRoute ∷ * → * where
  ApiRoute_ListCooperatives ∷ ApiRoute ()

data BackendRoute :: * → * where
  BackendRoute_ApiRoute ∷ BackendRoute (R ApiRoute)
  BackendRoute_Missing ∷ BackendRoute ()

-- Frontend

data FrontendRoute ∷ * → * where
  -- General Pages
  FrontendRoute_Main ∷ FrontendRoute ()
  FrontendRoute_About ∷ FrontendRoute ()

fullRouteEncoder ∷ Encoder (Either Text) Identity (R (FullRoute BackendRoute FrontendRoute)) PageName
fullRouteEncoder = mkFullRouteEncoder
  (FullRoute_Backend BackendRoute_Missing :/ ())
  (\case
      BackendRoute_Missing → PathSegment "missing" $ unitEncoder mempty
      BackendRoute_ApiRoute → PathSegment "api" $ pathComponentEncoder $ \case
        ApiRoute_ListCooperatives → PathSegment "cooperatives" $ unitEncoder mempty

  )
  (\case
      FrontendRoute_Main         → PathEnd $ unitEncoder mempty
      FrontendRoute_About        → PathSegment "acerca" $ unitEncoder mempty
  )

concat <$> mapM deriveRouteComponent
  [ ''ApiRoute
  , ''BackendRoute
  , ''FrontendRoute
  ]
