module LaTiendita.Frontend.MenuBar where

import Obelisk.Frontend
import Obelisk.Route.Frontend
import Reflex.Dom

import LaTiendita.Frontend.Common

frontendMenuLeft :: ObeliskWidget js t route m => RoutedT t route m ()
frontendMenuLeft = elAttr "ul" ("class" =: "menu") $ do
  el "li" $ elAttr "img" ("class" =: "logo" <> "id" =: "menu-logo" <>
      "src" =: cloudflareIpfsGateway "QmV6Ea4vVECrEHV3B3y8tPcfoNaQa4x3YchD4teBtJMUV5?filename=latiendita.logo.svg"
    ) blank
  el "li" $ elAttr "a" ("href" =: "/acerca") $ text "Acerca"
  el "li" $ elAttr "a" ("href" =: "/cooperativas") $ text "Cooperativas"

frontendMenuRight :: ObeliskWidget js t route m => RoutedT t route m ()
frontendMenuRight = elAttr "ul" ("class" =: "menu") $ do
  el "li" $ elAttr "input" ("type" =: "search" <> "placeholder" =: "Busqueda" <> "id" =: "latiendita-main-search") blank
  -- TODO: Checar sesion y desplegar cuenta/carrito si esta ingresado
  el "li" $ elAttr "a" ("class" =: "button") $ text "Ingresar"

frontendMenu :: ObeliskWidget js t route m => RoutedT t route m ()
frontendMenu =
  elAttr "div" ("id" =: "latiendita-menu-bar" <> "data-sticky-container" =: "") $
    elAttr "div" ("class" =: "top-bar" <> "data-sticky" =: "" <> "data-options" =: "margin-top:0;") $ do
      elAttr "div" ("class" =: "top-bar-left") frontendMenuLeft
      elAttr "div" ("class" =: "top-bar-right") frontendMenuRight
