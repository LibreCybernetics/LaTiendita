module LaTiendita.Frontend.Common where

import Data.Text

cloudflareIpfsGateway :: Text -> Text
cloudflareIpfsGateway cid = "https://cloudflare-ipfs.com/ipfs/" <> cid
