module LaTiendita.Frontend.Template where

import Obelisk.Frontend
import Obelisk.Generated.Static
import Obelisk.Route.Frontend
import Reflex.Dom

includeStyles :: ObeliskWidget js t route m => RoutedT t route m ()
includeStyles = do
  elAttr "link" (
      "href" =: "https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" <>
      "type" =: "text/css" <>
      "rel" =: "stylesheet"
    ) blank
  elAttr "link" ("href" =: static @"css/latiendita.css" <> "type" =: "text/css" <> "rel" =: "stylesheet") blank
