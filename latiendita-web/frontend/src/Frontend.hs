module Frontend where

import Obelisk.Frontend
import Obelisk.Route
import Obelisk.Route.Frontend

import Reflex.Dom

import LaTiendita.Common.Route
import LaTiendita.Frontend.MenuBar
import LaTiendita.Frontend.Template

--
-- Main
--

frontendHead :: ObeliskWidget js t route m => RoutedT t route m ()
frontendHead = do
  el "title" $ text "La Tiendita Cooperativa"
  includeStyles

frontendBody :: ObeliskWidget js t route m => RoutedT t route m ()
frontendBody = do
  frontendMenu
  elAttr "div" ("id" =: "latiendita-body-root" <> "class" =: "grid-container") $
    elAttr "div" ("class" =: "grid-x grid-margin-x") $ do
      elAttr "div" ("class" =: "cell small-4") $ el "h1" $
        text "En construcción"
      elAttr "div" ("class" =: "cell small-4") $ el "h2" $
        elAttr "a" ("href" =: "https://gitlab.com/LibreCybernetics/LaTiendita") $
          text "Repo"

-- This runs in a monad that can be run on the client or the server.
-- To run code in a pure client or pure server context, use one of the
-- `prerender` functions.
frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = frontendHead
  , _frontend_body = frontendBody
  }
