import Obelisk.Frontend
import Obelisk.Route.Frontend
import Reflex.Dom

import Frontend
import LaTiendita.Common.Route

main :: IO ()
main = do
  let Right validFullEncoder = checkEncoder fullRouteEncoder
  run $ runFrontend validFullEncoder frontend
