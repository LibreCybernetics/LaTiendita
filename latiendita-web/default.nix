{ obelisk ? import ./.obelisk/impl {
    system = builtins.currentSystem;
  }
}:
with obelisk;
project ./. ({ ... }: {
  packages = {
    latiendita = ../latiendita;
  };
})
