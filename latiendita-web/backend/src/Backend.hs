module Backend (backend) where

import Prelude hiding (lookup)

import Data.Aeson
import Data.Aeson.Text
import Data.Dependent.Sum (DSum (..))
import Data.Functor.Identity
import Data.Map.Strict
import Data.Maybe
import Data.Pool

import qualified Data.ByteString as BS
import qualified Data.Text          as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy     as LT

import Control.Applicative
import Control.Monad.Logger
import Control.Monad.Reader
import Control.Monad.IO.Unlift
import System.Environment

import Database.Persist.Sql
import Database.Persist.Postgresql
import Obelisk.Backend
import Obelisk.ExecutableConfig.Lookup
import Snap

import LaTiendita.Common.Route
import LaTiendita.Schema
import LaTiendita.Schema.CooperativeData

encodeToText :: ToJSON a => a -> T.Text
encodeToText = LT.toStrict . encodeToLazyText

writeJson :: ToJSON a => a -> Snap ()
writeJson val = do
  modifyResponse $ setContentType "application/json"
  writeText . encodeToText $ val
  return ()

createDatabasePool :: (MonadUnliftIO m, IsPersistBackend backend, BaseBackend backend ~ SqlBackend) =>
  ConnectionString -> (Pool backend -> LoggingT m a) -> m a
createDatabasePool database action = runStderrLoggingT $ withPostgresqlPool database 10 action

runDB' :: (MonadIO m, IsPersistBackend backend, BaseBackend backend ~ SqlBackend) =>
  Pool backend -> ReaderT backend IO a -> m a
runDB' pool action = liftIO $ runSqlPool action pool

notFoundRoute :: Snap ()
notFoundRoute = do
  writeBS "404"
  return ()

listCooperatives :: MonadIO m => (ReaderT SqlBackend m [Entity Cooperative] -> IO [Entity Cooperative]) -> Snap ()
listCooperatives runDB = do
  cooperatives <- liftIO $ runDB getCooperatives
  writeJson (entityVal <$> cooperatives)
  return ()

backend :: Backend BackendRoute FrontendRoute
backend = Backend
  { _backend_run = \serve -> do
      configs <- liftIO getConfigs

      -- DB Configuration
      maybeProductionDB <- fmap (T.encodeUtf8 . T.pack) <$> lookupEnv "PRODUCTION_DB"
      let maybeDatabase = maybeProductionDB <|> "backend/test_db" `lookup` configs
      let database :: BS.ByteString = fromMaybe "postgres://latiendita:latiendita@localhost:5432/latiendita" maybeDatabase

      -- Running
      createDatabasePool database $ \pool -> do

        -- If it is a test/development enviroment _do_ migration if it is a production environment print migrations
        liftIO $ case maybeProductionDB of
          Just _ -> mockMigration migrateAll
          Nothing -> runDB' pool $ runMigration migrateAll

        -- Main
        liftIO $ serve $ \case
          BackendRoute_Missing :=> Identity () -> notFoundRoute
          -- API Routes
          BackendRoute_ApiRoute :=> Identity sub -> case sub of
            ApiRoute_ListCooperatives :=> Identity () -> listCooperatives (runDB' pool)

  , _backend_routeEncoder = fullRouteEncoder
  }
