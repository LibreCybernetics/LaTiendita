# La Tiendita Cooperativa

Un sitio de comercio electronico para cooperativas locales

## Requisitos

`nix`: Del gerente de paquetes de tu sistema operativo o `curl -L https://nixos.org/nix/install | sh`

`stack`: Del gerente de paquetes de tu sistema operativo o `nix-env -iA nixpkgs.stack`

`ob`: https://github.com/obsidiansystems/obelisk#installing-obelisk


## Correr

- Librería (solo pruebas): en `latiendita` correr `stack test --file-watch`
- Sitio: en `latiendita-web` correr `ob run` (Recarga automaticamente cambios en `latiendita-web`)
- Documentación: en `latiendita-web` correr `ob hoogle -p [puerto]`


## Deplegar

1. `docker load < (nix-build -A dockerImage --no-out-link --argstr name "latiendita" --argstr version "202004111740")`
2. `docker tag latiendita:202004111740 registry.gitlab.com/librecybernetics/latiendita:202004111740`
3. `docker push registry.gitlab.com/librecybernetics/latiendita`
4. Actualizar `k8s/deployment.yaml` y `kubectl apply -f k8s/deployment.yaml`

Pendiente de escribir un script que cheque condiciones antes de desplegar

Caveats:
- Las imagenes se construyen por `nix.dockerTools.buildLayeredImage` y no por Docker.

## Requisitos opcionales

- Servicio de IPFS en la computadora e IPFS Companion en el navegador.

# Futuro

Sistema de Planeación Economica
