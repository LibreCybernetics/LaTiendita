module LaTiendita.Schema.PersonData where

import GHC.Generics

import Data.Aeson

import LaTiendita.Schema ( Account
                         , Cooperative
                         , Person, PersonId
                         , PersonName
                         )

data PersonData = PersonData {
      personDataPersonId           :: PersonId
    , personDataPerson             :: Person
    , personDataPersonName         :: PersonName
    , personDataPersonAccounts     :: [Account]
    , personDataPersonCooperatives :: [Cooperative]
  } deriving (Generic, Show)

instance ToJSON   PersonData
instance FromJSON PersonData
