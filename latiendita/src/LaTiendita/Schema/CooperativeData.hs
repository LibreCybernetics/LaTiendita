module LaTiendita.Schema.CooperativeData where

import GHC.Generics

import Control.Monad.IO.Class
import Control.Monad.Reader

import Data.Aeson

import Database.Persist
import Database.Persist.Sql

import LaTiendita.Schema

data CooperativeData = CooperativeData {
    cooperativeDataId          ∷ CooperativeId
  , cooperativeDataCooperative ∷ Cooperative
  , cooperativeDataBrands      ∷ [Brand]
  , cooperativeDataMembers     ∷ [Person]
  } deriving (Generic, Show)

instance ToJSON   CooperativeData
instance FromJSON CooperativeData

getCooperatives ∷ MonadIO m ⇒ ReaderT SqlBackend m [Entity Cooperative]
getCooperatives = selectList [] []

getCooperativeBrands ∷ MonadIO m ⇒ Cooperative → ReaderT SqlBackend m [Entity Brand]
getCooperativeBrands coop = do
  maybeCooperative ← getBy $ UniqueCooperative (cooperativeShortName coop)
  brandCooperative ← case maybeCooperative of
    Nothing          → return []
    Just cooperative → undefined cooperative
  undefined brandCooperative
