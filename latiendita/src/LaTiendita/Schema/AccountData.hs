module LaTiendita.Schema.AccountData where

import Control.Monad.IO.Class
import Control.Monad.Reader

import Data.Time.Clock

import Database.Persist
import Database.Persist.Sql

import LaTiendita.Schema

getOrCreateNewAccount :: MonadIO m => Account -> ReaderT SqlBackend m (Key Account)
getOrCreateNewAccount account = do
  maybeExistingAccount <- getBy $ UniqueAccount (accountOauthProvider account) (accountOauthIdentity account)
  case maybeExistingAccount of
    Just existingAccount -> pure . entityKey $ existingAccount
    Nothing -> do
      currentTime <- liftIO getCurrentTime
      newPersonId <- insert $ Person currentTime
      let accountWithPerson = account { accountPersonId = Just newPersonId }
      insert accountWithPerson
