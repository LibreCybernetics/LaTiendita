{-# LANGUAGE QuasiQuotes #-}
module LaTiendita.Schema where

import GHC.Generics
import Prelude.Unicode

import Data.Aeson
import Data.Text
import Data.Time

import Database.Persist.TH

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Account
  -- Owner
  personId          PersonId Maybe
  -- Data
  oauthProvider     Text
  oauthIdentity     Text
  oauthRefreshToken Text Maybe
  oauthAccessToken  Text Maybe
  createdAt         UTCTime
  -- Constraints
  UniqueAccount oauthProvider oauthIdentity
  deriving Generic Show

Brand
  -- Owner
  cooperativeId CooperativeId
  -- Data
  name      Text
  createdAt UTCTime
  -- Constraints
  UniqueBrand name
  deriving Generic Show

Cooperative
  shortName   Text
  legalName   Text
  logoIpfsCid Text
  description Text
  role        Text Maybe
  createdAt   UTCTime Maybe
  -- Constraints
  UniqueCooperative shortName
  deriving Generic Show

Person
  createdAt UTCTime
  deriving Generic Show

PersonName
  personId PersonId
  name  Text
  since UTCTime
  deriving Generic Show

PersonCooperative
  personId      PersonId
  cooperativeId CooperativeId
  isPriviledged Bool
  -- Constraints
  UniquePersonCooperative personId cooperativeId
  deriving Generic Show

Resource
  resourceSpecId ResourceSpecId
  lot            Text Maybe
  lotDate        Day  Maybe
  createdAt      UTCTime
  deriving Generic Show

ResourceClass
  name Text
  UniqueResourceClassification name
  deriving Generic Show

ResourceClassRel
  resourceClassId ResourceClassId
  subclassOf      ResourceClassId
  UniqueResourceClassSubclass resourceClassId subclassOf
  deriving Generic Show

ResourceSpec
  brandId        BrandId
  primaryClassId ResourceClassId
  name           Text
  description    Text
  deriving Generic Show
|]

instance Eq PersonName where
  a == b = sameId
    where
      sameId = personNamePersonId a ≡ personNamePersonId b

instance ToJSON   Account
instance FromJSON Account

instance ToJSON   Brand
instance FromJSON Brand

instance ToJSON   Cooperative where
  toJSON (Cooperative shortName legalName logoIpfsCid description _ _) =
    object [ "shortName" .= shortName
           , "legalName" .= legalName
           , "logoIpfsCid" .= logoIpfsCid
           , "description" .= description
           ]
instance FromJSON Cooperative

instance ToJSON   Person
instance FromJSON Person

instance ToJSON   PersonName
instance FromJSON PersonName
