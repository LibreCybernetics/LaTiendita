module LaTiendita.Schema.PersonTest where

import Test.Tasty.Hspec

import Data.Time.Clock

import Database.Persist
import Database.TestHelpers

import LaTiendita.Schema

spec_basic :: Spec
spec_basic = withTestDB $
    describe "Simple Inserts and Selects" $
      it "Insert one person" $ \conn -> do
        let name = "Fabián"
        currentTime  <- getCurrentTime
        personId     <- runDB conn . insert $ Person currentTime
        personNameId <- runDB conn . insert $ PersonName personId name currentTime
        Just personName <- runDB conn $ get personNameId
        personName `shouldBe` PersonName personId name currentTime
