module Database.TestHelpers where

import Test.Tasty.Hspec

import Control.Monad.Logger
import Control.Monad.Reader

import Database.Persist.Sql
import Database.Persist.Sqlite
import Database.Sqlite

import LaTiendita.Schema

openConnection :: LogFunc -> IO SqlBackend
openConnection logfn = do
  conn <- open ":memory:"
  wrapConnection conn logfn

runDB :: SqlBackend -> ReaderT SqlBackend (LoggingT IO) a -> IO a
runDB conn queries = runStderrLoggingT $ runSqlConn queries conn

withTestDB' :: ActionWith SqlBackend -> IO ()
withTestDB' test = runStderrLoggingT $
  withSqlConn openConnection $ \conn -> do
    liftIO $ runDB conn (runMigration migrateAll)
    liftIO $ test conn

withTestDB :: SpecWith SqlBackend -> Spec
withTestDB = around withTestDB'
